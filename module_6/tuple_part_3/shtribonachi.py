n = int(input())
tribonacci = [1, 1, 1]
if n == 1:
    print(1)
elif n == 2:
    print(*[1, 1])
elif n == 3:
    print(*[1, 1, 1])
else:
    for i in range(3, n):
        tribonacci.append(tribonacci[i-1] + tribonacci[i-2] + tribonacci[i-3])
    print(*tribonacci)