

def make_list(size):
    return [list(input().split()) for _ in range(size)]

row_count = int(input())

students = make_list(row_count)

for name, grade in students:
    print(name, grade)

print()
for name, grade in students:
    if int(grade) >= 4:
        print(name, grade)




