

def high_point(a, b, c):
    x = -b / (2 * a)
    y = c - b ** 2 / (4 * a)
    return x, y


a, b, c = int(input()), int(input()), int(input())

print(high_point(a, b, c))