tuples = [(10, 20, 40), (40, 50, 60), (70, 80, 90), (10, 90), (1, 2, 3, 4), (5, 6, 10, 2, 1, 77)]
new_tuples = []
for x in tuples:
    new_x = list(x)
    new_x[-1] = 100
    new_tuples.append(new_x)
new_tuples = [tuple(x) for x in new_tuples]
print(new_tuples)