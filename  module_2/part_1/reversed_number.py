"""
Дано пятизначное или шестизначное натуральное число.
Напишите программу, которая изменит порядок его последних пяти цифр на обратный.
"""

your_number = input()

if len(your_number) == 5:
    res = your_number[::-1]
    res.strip('0')
    print(res.strip('0'))
else:
    res = your_number[1:6]
    print(f'{your_number[0]}{res[::-1]}')
