"""
n человек, пронумерованных числами от
1 до n, стоят в кругу. Они начинают считаться, каждый
k-й по счету человек выбывает из круга, после чего счет продолжается
со следующего за ним человека. Напишите программу,
определяющую номер человека, который останется в кругу последним.
"""


def remove_person(people_count, remove_number):
    return 0 \
        if people_count == 0 \
        else (remove_person(people_count - 1, remove_number) + remove_number) % people_count


people_count, remove_number = int(input()), int(input())
print(remove_person(people_count, remove_number) + 1)
