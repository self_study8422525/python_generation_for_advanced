"""
Дан набор точек на координатной плоскости. Необходимо подсчитать и
вывести количество точек, лежащих в каждой координатной четверти.
"""

points = []
for _ in range(int(input())):
    new_point = tuple(map(int, input().split()))
    points.append(new_point)


def first_quart(points):
    total = 0
    for item in points:
        if item[0] > 0 and item[1] > 0:
            total += 1
    return total


def second_quart(points):
    total = 0
    for item in points:
        if item[0] < 0 and item[1] > 0:
            total += 1
    return total


def third_quart(points):
    total = 0
    for item in points:
        if item[0] < 0 and item[1] < 0:
            total += 1
    return total


def fourth_quart(points):
    total = 0
    for item in points:
        if item[0] > 0 and item[1] < 0:
            total += 1
    return total

print(f'Первая четверть: {first_quart(points)}\n'
      f'Вторая четверть: {second_quart(points)}\n'
      f'Третья четверть: {third_quart(points)}\n'
      f'Четвертая четверть: {fourth_quart(points)}')

"""
4
0 -1
1 2
0 9
-9 -5
"""