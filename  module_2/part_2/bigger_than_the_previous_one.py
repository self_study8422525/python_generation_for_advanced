"""
На вход программе подается строка текста из натуральных чисел.
Из неё формируется список чисел. Напишите программу подсчета количества чисел,
которые больше предшествующего им в этом списке числа.
"""

numbers = [int(x) for x in input().split()]

total = 0
for num in range(1, len(numbers)):
    if numbers[num] > numbers[num - 1]:
        total += 1

print(total)

