"""
Напишите программу для определения, является ли число
произведением двух чисел из данного набора, выводящую
результат в виде ответа «ДА» или «НЕТ».
"""

# numbers = [int(input()) for x in range(int(input()))]
# length = len(numbers)
# expected = int(input())
#
# if length > 1:
#     result = [numbers[x] * numbers[y] for x in range(length)
#               for y in range(length) if x is not y]
#     if expected in result:
#         print("ДА")
#     else:
#         print("НЕТ")
# else:
#     print("НЕТ")

import random, timeit

numbers = random.sample(range(0, 30000), 5000)
length = len(numbers)
expected = max(numbers)
print(expected)


def division():
    if length > 1:
        for number in numbers:
            if expected > number and number != 0 and not expected % number and (expected / number) in numbers and (number != (expected / number) or numbers.count(number) > 1) or (expected == 0 and 0 in numbers):
                print("ДА")
                break
        else:
            print("НЕТ")
    else:
        print("НЕТ")


def multiplication():
    if length > 1:
        result = [numbers[x] * numbers[y] for x in range(length)
                  for y in range(length) if x is not y]
        if expected in result:
            print("ДА")
        else:
            print("НЕТ")
    else:
        print("НЕТ")


if __name__ == '__main__':
    start = timeit.default_timer()
    division()
    print(f'{timeit.default_timer() - start}')
    start = timeit.default_timer()
    multiplication()
    print(f'{timeit.default_timer() - start}')