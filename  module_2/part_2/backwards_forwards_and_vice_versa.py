"""
На вход программе подается строка текста из натуральных чисел.
Из элементов строки формируется список чисел. Напишите программу,
которая меняет местами соседние элементы списка (a[0] c a[1], a[2] c a[3] и т.д.).
Если в списке нечетное количество элементов, то последний остается на своем месте.
"""

numbers = [int(x) for x in input().split()]
length = len(numbers)

if length == 1:
    print(*numbers)
elif length % 2 != 0:
    odd_nums = numbers[:length - 1]

    for num in range(0, len(odd_nums) - 1, 2):
        odd_nums[num], odd_nums[num + 1] = odd_nums[num + 1], odd_nums[num]
    odd_nums.append(numbers[length - 1])
    print(*odd_nums)
else:
    for num in range(0, length - 1, 2):
        numbers[num], numbers[num + 1] = numbers[num + 1], numbers[num]
    print(*numbers)