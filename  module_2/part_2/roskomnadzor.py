import re
word = input() + ' запретил букву'

for char in sorted(set(word.replace(' ', ''))):
    if char == 'ё':
        continue
    if len(word.replace(' ', '')) == 0:
        break
    print(''.join(re.findall(r'\w+\s?', f'{word.strip()} {char}')))
    word = (word.replace(char, ''))


