import string
import re

substrings = [x + 1 for x in range(int(input())) if re.match(r'.*a.*n.*t.*o.*n.*', input())]

print(*substrings)
