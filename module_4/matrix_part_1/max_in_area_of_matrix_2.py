

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def side_triangles(matrix):
    length = len(matrix)
    result = []
    for col in range(length):
        for row in range(length):
            if col == row:
                result.append(matrix[col][row])
            elif row == 0 or row == length - 1:
                result.append(matrix[col][row])
            elif col >= row and col < length - 1 - row:
                result.append(matrix[col][row])
            elif col <= row and col > length - 1 - row:
                result.append(matrix[col][row])

    return max(result)

matrix = make_matrix(int(input()))
print(side_triangles(matrix))
# print(max_area(matrix))
