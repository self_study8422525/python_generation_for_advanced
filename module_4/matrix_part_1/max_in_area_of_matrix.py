

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def max_area(matrix):
    elements = []
    counter = 0
    for row in matrix:
        for elem in row[:counter+1]:
            elements.append(elem)
        counter += 1
    return max(elements)


matrix = make_matrix(int(input()))
print(max_area(matrix))
