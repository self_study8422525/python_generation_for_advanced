
def make_matrix(row, column):
    return [[input() for col in range(column)] for r in range(row)]


def reverse_matrix(matrix):
    reversed_matrix = []
    counter = 0

    for i in range(len(matrix[0])):
        new_column = []
        for r in range(len(matrix)):
            new_column.append(matrix[r][counter])
        reversed_matrix.append(new_column)
        counter += 1

    return reversed_matrix


def print_matrix(matrix_1, matrix_2):
    for r in matrix_1:
        print(*r)

    print()

    for r in matrix_2:
        print(*r)


matrix = make_matrix(int(input()), int(input()))

rev = reverse_matrix(matrix)
print_matrix(matrix, rev)

