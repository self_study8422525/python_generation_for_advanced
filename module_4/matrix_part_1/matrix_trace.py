# matrix = [list(map(int, input().split())) for _ in range(int(input()))]


def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def get_trace(input_matrix):
    trace, counter = [], 0
    for row in range(len(input_matrix)):
        trace.append(input_matrix[row][counter])
        counter += 1
    return sum(trace)


matrix = make_matrix(int(input()))
print(get_trace(matrix))