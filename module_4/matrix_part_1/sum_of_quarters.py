
def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def sum_of_quarters(matrix):
    length = len(matrix)
    sums = [0, 0, 0, 0]
    for col in range(length):
        for row in range(length):
            # верхняя
            if col < row and col < length - 1 - row:
                sums[0] += matrix[col][row]
            # правая
            if col < row and col > length - 1 - row:
                sums[1] += matrix[col][row]
            # нижняя
            if col > row and col > length - 1 - row:
                sums[2] += matrix[col][row]
            # левая
            if col > row and col < length - 1 - row:
                sums[3] += matrix[col][row]
    return sums

def print_result(input_list):
    print(f'Верхняя четверть: {input_list[0]}\n'
          f'Правая четверть: {input_list[1]}\n'
          f'Нижняя четверть: {input_list[2]}\n'
          f'Левая четверть: {input_list[3]}\n')

matrix = make_matrix(int(input()))
print_result(sum_of_quarters(matrix))
