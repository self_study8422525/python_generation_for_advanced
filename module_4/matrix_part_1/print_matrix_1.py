row = int(input())
column = int(input())

matrix = [[input() for col in range(column)] for r in range(row)]

for r in matrix:
    print(*r)
