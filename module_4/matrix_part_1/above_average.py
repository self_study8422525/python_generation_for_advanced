

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def above_average(matrix):
    result = []
    for row in matrix:
        res_row = []
        row_sum = sum(row)
        row_average = row_sum / len(row)
        for num in row:
            if num > row_average:
                res_row.append(num)
        result.append(len(res_row))
    return result


def print_result(res_list):
    for res in res_list:
        print(res)

matrix = make_matrix(int(input()))
print_result(above_average(matrix))
