your_string, chunk = input().split(), int(input())


def chunked(raw, num):
    packed = []
    first, second = 0, num
    while first < len(raw):
        if first >= len(raw):
            packed.append(raw[first:])
            break
        packed.append(raw[first:second])
        first += num
        second += num
    return packed


print(chunked(your_string, chunk))
