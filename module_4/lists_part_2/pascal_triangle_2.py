your_number = int(input())


def pascal_tri(num):
    triangle = []
    for elem in range(num + 1):
        coefficient = 1
        res = []
        for item in range(1, elem + 1):
            res.append(coefficient)
            coefficient = coefficient * (elem + 1 - item) // item
        triangle.append(res + [1])

    return triangle


def print_triangle(triangle):
    for item in range(len(triangle) - 1):
        print(*triangle[item])


print_triangle(pascal_tri(your_number))
