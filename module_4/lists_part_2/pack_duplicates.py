your_string = input().split()


def pack(raw):
    packed = []
    current_char = raw[0]
    current_group = [current_char]

    for char in raw:
        if char == current_char:
            current_group.append(char)
        else:
            packed.append(current_group)
            current_group = [char]
            current_char = char
    packed.append(current_group)
    return packed


print(pack(your_string))