number = int(input())


def list_by_pattern(num):
    total = 0
    while total != num:
        total += 1
        print( f'{list_generator(total)}')


def list_generator(num):
    return [x + 1 for x in range(num)]


list_by_pattern(number)