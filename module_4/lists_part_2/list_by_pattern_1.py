number = int(input())


def list_by_pattern(num):
    return f'{list_generator(int(num))}\n' * int(num)


def list_generator(num):
    return [x + 1 for x in range(num)]


print(list_by_pattern(number))

