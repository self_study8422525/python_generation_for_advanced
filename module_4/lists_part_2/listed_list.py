your_string = input().split()


def listed_list(raw):
    lists = []

    for element in range(len(raw)):
        for item in range(element + 1, len(raw) + 1):
            lists.append(raw[element:item])

    sorted_lists = sorted(lists, key=len)
    sorted_lists.insert(0, [])
    return sorted_lists


print(listed_list(your_string))