def make_matrix(num_rows, num_cols):
    return [[0 for _ in range(num_cols)] for _ in range(num_rows)]


def filled_matrix(input_matrix):
    for r in range(len(input_matrix)):
        for c in range(len(input_matrix[r])):
            matrix[r][c] = (r + c) % len(input_matrix[r]) + 1
    return input_matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)
for row in filled_matrix(matrix):
    print(*row)