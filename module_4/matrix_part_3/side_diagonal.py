

def make_matrix(size):
    return [[0] * size for _ in range(size)]


def with_diagonal(input_matrix):
    length = len(input_matrix)
    for row in range(length):
        for col in range(len(input_matrix[row])):
            if row + col == length - 1:
                input_matrix[row][col] = 1
            if row + col >= length:
                input_matrix[row][col] = 2
    return input_matrix


for row in with_diagonal(make_matrix(int(input()))):
    print(*row)