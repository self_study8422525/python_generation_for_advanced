def make_matrix(size):
    return [[0 for _ in range(size)] for _ in range(size)]


def filled_matrix(input_matrix):
    length = len(input_matrix)
    for r in range(length):
        for c in range(len(input_matrix[r])):
            if r == c:
                input_matrix[r][r] = 1
            if r + c == length - 1:
                input_matrix[r][c] = 1
    return input_matrix


matrix = make_matrix(int(input()))
for row in filled_matrix(matrix):
    print(*row)

