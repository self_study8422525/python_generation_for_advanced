def make_matrix(num_rows, num_cols):
    return [[0 for _ in range(num_cols)] for _ in range(num_rows)]


def diagonal_filled(input_matrix):
    num_rows = len(input_matrix)
    num_cols = len(input_matrix[0])
    current_number = 1
    for i in range(num_rows + num_cols - 1):
        start_row = max(0, i - num_cols + 1)
        end_row = min(i, num_rows - 1)
        for j in range(start_row, end_row + 1):
            input_matrix[j][i - j] = current_number
            current_number += 1
    return input_matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)

for row in diagonal_filled(matrix):
    print(*row)