def make_matrix(num_rows, num_cols):
    return [[0 for _ in range(num_cols)] for _ in range(num_rows)]


def snake_filled(input_matrix):
    for r in range(len(input_matrix)):
        length_cols = len(input_matrix[r])
        for c in range(length_cols):
            if r % 2 == 0:
                input_matrix[r][c] = r * length_cols + c + 1
            else:
                input_matrix[r][c] = r * length_cols + (length_cols - c - 1) + 1
    return input_matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)

for row in snake_filled(matrix):
    print(*row)
