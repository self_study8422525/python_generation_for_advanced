def make_matrix(r, c):
    return [[0 for _ in range(c)] for _ in range(r)]


def filled_matrix(input_matrix):
    for r in range(len(input_matrix)):
        for c in range(len(input_matrix[r])):
            input_matrix[r][c] = r * len(input_matrix[r]) + c + 1
    return input_matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)
for row in filled_matrix(matrix):
    print(*row)
