
def make_board(row, col):
    return [['.'] * col for _ in range(row)]

def chess_board(input_matrix):
    for row in range(len(input_matrix)):
        for col in range(len(input_matrix[row])):
            if row % 2 != 0 and col % 2 == 0:
                input_matrix[row][col] = '*'
            if row % 2 == 0 and col % 2 != 0:
                input_matrix[row][col] = '*'
    return input_matrix

row, col = list(map(int, input().split()))

empty_board = make_board(row, col)

for row in chess_board(empty_board):
    print(*row)
# print(chess_board(chess))
