def make_matrix(num_rows, num_cols):
    return [[0 for _ in range(num_cols)] for _ in range(num_rows)]


def spiral_filled(input_matrix):
    num_rows = len(input_matrix)
    num_cols = len(input_matrix[0])
    current_number = 1
    row_start = 0
    row_end = num_rows - 1
    col_start = 0
    col_end = num_cols - 1

    while row_start <= row_end and col_start <= col_end:
        for j in range(col_start, col_end + 1):
            input_matrix[row_start][j] = current_number
            current_number += 1
        row_start += 1

        for i in range(row_start, row_end + 1):
            input_matrix[i][col_end] = current_number
            current_number += 1
        col_end -= 1

        if row_start <= row_end:
            for j in range(col_end, col_start - 1, -1):
                input_matrix[row_end][j] = current_number
                current_number += 1
            row_end -= 1

        if col_start <= col_end:
            for i in range(row_end, row_start - 1, -1):
                input_matrix[i][col_start] = current_number
                current_number += 1
            col_start += 1

    return input_matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)

for row in spiral_filled(matrix):
    print(*row)