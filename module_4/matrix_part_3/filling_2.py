def make_matrix(num_rows, num_cols):
    return [[0 for j in range(num_cols)] for i in range(num_rows)]


def filled_matrix(input_matrix):
    for r in range(len(input_matrix)):
        for c in range(len(input_matrix[r])):
            matrix[r][c] = r + c * len(input_matrix) + 1
    return matrix


rows, cols = list(map(int, input().split()))
matrix = make_matrix(rows, cols)
for row in filled_matrix(matrix):
    print(*row)
