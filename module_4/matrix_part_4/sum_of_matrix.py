def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def sum_matrix(matrix_1, matrix_2):
    result = [[0 for _ in range(len(matrix_1[0]))] for _ in range(len(matrix_1))]
    for r in range(len(matrix_1)):
        for row in range(len(matrix_1[r])):
            result[r][row] = matrix_1[r][row] + matrix_2[r][row]
    return result


matrix_A = make_matrix(3)
matrix_B = make_matrix(3)

print(sum_matrix(matrix_A, matrix_B))
