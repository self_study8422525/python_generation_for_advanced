def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def k_times(matrix_1, k):
    result = [[0 for _ in range(len(matrix_1[0]))] for _ in range(len(matrix_1))]
    for r in range(len(matrix_1)):
        for row in range(len(matrix_1[r])):
            result[r][row] = matrix_1[r][row] * k
    return result

matrix_A = make_matrix(3)
print(k_times(matrix_A, int(input())))