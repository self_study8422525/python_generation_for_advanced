

def make_matrix(rows, cols):
    return [list(map(int, input().split()))[:cols] for _ in range(rows)]


def multiply_matrices(matrix_1, matrix_2):
    result = [[0 for _ in range(len(matrix_2[0]))] for _ in range(len(matrix_1))]

    for row in range(len(matrix_1)):
        for col in range(len(matrix_2[0])):
            for k in range(len(matrix_2)):
                result[row][col] += matrix_1[row][k] * matrix_2[k][col]

    return result


n, m = list(map(int, input().split()))
matrix_1 = make_matrix(n, m)

input()
l, k = list(map(int, input().split()))
matrix_2 = make_matrix(l, k)


for row in multiply_matrices(matrix_1, matrix_2):
    print(*row)
