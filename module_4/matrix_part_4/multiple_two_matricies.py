# def make_matrix(row, column):
#     return [[input().split() for col in range(column)] for r in range(row)]
    # return [list(map(int, input().split())) for _ in range(size)]

# def multiply_matrices(matrix_1, matrix_2):
#     result = [[0 for _ in range(len(matrix_2[0]))] for _ in range(len(matrix_1))]
#
#     for row in range(len(matrix_1)):
#         for col in range(len(matrix_1[row])):
#             for k in range(len(matrix_2)):
#                 result[row][col] += matrix_1[row][k] * matrix_2[k][col]
#
#     return result

def multiply_matrices(matrix1, matrix2):

    result = [[0 for j in range(len(matrix2[0]))] for i in range(len(matrix1))]

    for i in range(len(matrix1)):
        for j in range(len(matrix2[0])):
            for k in range(len(matrix2)):
                result[i][j] += matrix1[i][k] * matrix2[k][j]

    return result


matrix_A = [[1, -1, 0], [3, -4, 2]]
matrix_B = [[1, 2], [3, 4], [5, 6]]


print(multiply_matrices(matrix_A, matrix_B))

# print(multiply_matrices(matrix_A, matrix_B))

'''
1 1 1
2 2 2
3 3 3
1 2 3
4 5 6
7 8 9
'''

"""
1 -1 0
3 -4 2
1 2
3 4
5 6
"""