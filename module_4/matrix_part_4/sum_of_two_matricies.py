def make_matrix(row, column):
    return [list(map(int, input().split()))[:column] for r in range(row)]


def sum_matrix(matrix_1, matrix_2):
    result = [[0 for _ in range(len(matrix_1[0]))] for _ in range(len(matrix_1))]
    for r in range(len(matrix_1)):
        for row in range(len(matrix_1[r])):
            result[r][row] = matrix_1[r][row] + matrix_2[r][row]
    return result


rows, cols = list(map(int, input().split()))
matrix_1 = make_matrix(rows, cols)
input()
matrix_2 = make_matrix(rows, cols)

for row in sum_matrix(matrix_1, matrix_2):
    print(*row)



'''
2 4
1 2 3 4
5 6 7 1

3 2 1 2
1 3 1 3
'''