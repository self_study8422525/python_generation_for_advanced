def power(input_matrix, number):
    power_result = [[0 for _ in range(len(input_matrix))] for _ in range(len(input_matrix))]
    for row in range(len(input_matrix)):
        power_result[row][row] = 1

    while number > 0:
        if number % 2 == 1:
            power_result = multiply_matrices(power_result, input_matrix)
        input_matrix = multiply_matrices(input_matrix, input_matrix)
        number //= 2

    return power_result

def multiply_matrices(matrix_1, matrix_2):
    new_matrix = [[0 for _ in range(len(matrix_2[0]))] for _ in range(len(matrix_1))]

    for row in range(len(matrix_1)):
        for col in range(len(matrix_2[0])):
            for k in range(len(matrix_2)):
                new_matrix[row][col] += matrix_1[row][k] * matrix_2[k][col]

    return new_matrix


matrix = [[1, 0], [4, 1]]
result = power(matrix, 25)
print(result)