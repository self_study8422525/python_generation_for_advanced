

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def is_magic(input_matrix):
    length = len(input_matrix)
    total_sum = sum(range(1, length ** 2 + 1))

    first_element = input_matrix[0][0]
    if all(input_matrix[i][j] == first_element for i in range(length) for j in range(length)):
        return "NO"

    for row in input_matrix:
        row_sum = sum(row)
        if row_sum != total_sum // length:
            return "NO"

    for j in range(length):
        col_sum = sum(input_matrix[i][j] for i in range(length))
        if col_sum != total_sum // length:
            return "NO"

    diag_sum_1 = sum(input_matrix[i][i] for i in range(length))
    diag_sum_2 = sum(input_matrix[i][length - i - 1] for i in range(length))

    if diag_sum_1 != total_sum // length or diag_sum_2 != total_sum // length:
        return "NO"

    if len(set(num for row in input_matrix for num in row)) != length ** 2:
        return "NO"

    return "YES"


matrix = make_matrix(int(input()))
print(is_magic(matrix))
