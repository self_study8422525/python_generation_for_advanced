

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def diagonal_swap(input_matrix):
    length = len(input_matrix)

    for i in range(length):
        h = input_matrix[i][i]
        input_matrix[i][i] = input_matrix[length - 1 - i][i]
        input_matrix[length - 1 - i][i] = h
    return input_matrix


matrix = make_matrix(int(input()))
changed_matrix = diagonal_swap(matrix)


for r in changed_matrix:
    print(*r)
