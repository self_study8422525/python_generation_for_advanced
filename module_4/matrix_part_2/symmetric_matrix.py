

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def is_symmetric(input_matrix):
    length = len(input_matrix)

    for row in range(length):
        for col in range(row + 1, length):
            if input_matrix[row][col] != matrix[col][row]:
                return False
    return True


matrix = make_matrix(int(input()))

if is_symmetric(matrix):
    print("YES")
else:
    print("NO")