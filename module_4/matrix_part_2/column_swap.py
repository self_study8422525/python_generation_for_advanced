

def make_matrix(row, column):
    return [list(map(int, input().split()))[:column] for r in range(row)]


def swap_columns(input_matrix, col_1, col_2):
    for row in range(len(input_matrix)):
        matrix[row][col_1], matrix[row][col_2] = matrix[row][col_2], matrix[row][col_1]
    return input_matrix


matrix = make_matrix(int(input()), int(input()))
columns = list(map(int, input().split()))


def print_matrix(input_matrix):
    for r in input_matrix:
        print(*r)


print_matrix(swap_columns(matrix, columns[0], columns[1]))
# print(columns)