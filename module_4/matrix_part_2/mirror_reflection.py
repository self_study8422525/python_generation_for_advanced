

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def mirrored(input_matrix):
    length = len(input_matrix)

    for i in range(length - 1, -1, -1):
        print(*input_matrix[i])


matrix = make_matrix(int(input()))
mirrored(matrix)