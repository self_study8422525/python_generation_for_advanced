

def make_matrix(row, column):
    return [list(map(int, input().split()))[:column] for r in range(row)]


def max_in_matrix(input_matrix):
    max_element = max(max(input_matrix, key=max))
    result = []
    for row in range(len(input_matrix)):
        for col in range(len(input_matrix[row])):
            if max_element == input_matrix[row][col]:
                return [row, col]


matrix = make_matrix(int(input()), int(input()))

print(*max_in_matrix(matrix))