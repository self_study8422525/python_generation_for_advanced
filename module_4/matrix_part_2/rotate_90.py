

def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def rotate_90(input_matrix):
    n = len(input_matrix)
    m = len(input_matrix[0])
    new_matrix = [[0] * n for _ in range(m)]

    for i in range(n):
        for j in range(m):
            new_matrix[j][n - i - 1] = input_matrix[i][j]

    return new_matrix


matrix = make_matrix(int(input()))

for row in rotate_90(matrix):
    print(*row)