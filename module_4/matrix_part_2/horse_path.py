

def make_board():
    return [['.' for j in range(8)] for i in range(8)]


def horse_path(board, pos):
    row, col = int(pos[1])-1, ord(pos[0])-ord('a')
    board[row][col] = 'N'

    for dr, dc in [(2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1)]:
        r, c = row + dr, col + dc
        if 0 <= r < 8 and 0 <= c < 8:
            board[r][c] = '*'

    return board

pos = input()
board = make_board()
paths = horse_path(board, pos)

for row in range(len(paths) - 1, -1, -1):
    print(*paths[row])