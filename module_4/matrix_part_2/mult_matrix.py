

def make_matrix(row, column):
    return [[0 for col in range(column)] for r in range(row)]


def mult_matrix(input_matrix):
    multed = []
    for col in range(len(input_matrix)):
        resulted_row = []
        for row in range(len(input_matrix[col])):
            res = col * row
            resulted_row.append(res)
        multed.append(resulted_row)
    return multed


def print_matrix(input_matrix):
    for r in input_matrix:
        print(*r)


matrix = make_matrix(int(input()), int(input()))
print_matrix(mult_matrix(matrix))
