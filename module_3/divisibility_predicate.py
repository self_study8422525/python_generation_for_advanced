"""
Напишите функцию func(num1, num2), принимающую в качестве аргументов
два натуральных числа num1 и num2 и возвращающую значение True если
число num1 делится без остатка на число num2 и False в противном случае.
"""


def func(num_1, num_2):
    if num_1 == 0 or num_2 == 0:
        return "не делится"
    return "делится" if num_1 % num_2 == 0 else "не делится"

num_1, num_2 = int(input()), int(input())
print(func(num_1, num_2))

