def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def is_latin(input_matrix):
    n = len(input_matrix)
    sorted_rows = [sorted(row) == list(range(1, n+1)) for row in input_matrix]
    sorted_cols = [sorted(col) == list(range(1, n+1)) for col in zip(*input_matrix)]
    return all(sorted_rows) and all(sorted_cols)


matrix = make_matrix(int(input()))

if is_latin(matrix):
    print("YES")
else:
    print("NO")