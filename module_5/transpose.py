def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def transpose(matrix):
    new_matrix = [[0 for _ in range(len(matrix))] for _ in range(len(matrix[0]))]

    for row in range(len(matrix)):
        for col in range(len(matrix[0])):
            new_matrix[col][row] = matrix[row][col]

    return new_matrix


matrix = make_matrix(int(input()))
for row in transpose(matrix):
    print(*row)