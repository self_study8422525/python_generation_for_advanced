def make_matrix(size):
    return [['.' for _ in range(size)] for _ in range(size)]


def make_snowflake(input_matrix):
    mid_row = len(input_matrix) // 2
    mid_col = len(input_matrix[0]) // 2

    for row in range(len(input_matrix)):
        for col in range(len(input_matrix[row])):
            if row == col:
                input_matrix[row][col] = '*'
            if row == col or row + col == len(input_matrix) - 1:
                input_matrix[row][col] = '*'
            if input_matrix[mid_row][col] == '.':
                input_matrix[mid_row][col] = '*'
            if input_matrix[row][mid_col] == '.':
                input_matrix[row][mid_col] = '*'
    return input_matrix


matrix = make_matrix(int(input()))

for row in make_snowflake(matrix):
    print(*row)