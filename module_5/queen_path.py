def make_board():
    return [['.' for _ in range(8)] for _ in range(8)]


def convert_move_to_coords(move):
    col = ord(move[0]) - ord('a')
    row = int(move[1]) - 1
    return row, col


def mark_attack(board, move):
    row, col = convert_move_to_coords(move)
    n = len(board)
    for i in range(n):
        board[row][i] = "*"
        board[i][col] = "*"
    for i in range(n):
        for j in range(n):
            if i + j == row + col or i - j == row - col:
                board[i][j] = "*"
    board[row][col] = "Q"
    return board



move = input()
empty_board = make_board()


for row in mark_attack(empty_board, move)[::-1]:
    print(*row)