# def make_matrix(size):
#     return [list(map(int, input().split())) for _ in range(size)]


def parallel_diagonals(size):
    return [[abs(row - col) for col in range(size)] for row in range(size)]


for row in parallel_diagonals(int(input())):
    print(*row)