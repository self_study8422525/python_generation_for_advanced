input_list, elem = input().split(), int(input())
new_list = []

total = 0

for e in range(elem):
    result = [input_list[x] for x in range(len(input_list)) if (x + e) % elem == 0]
    new_list.append(result)

print(sorted(new_list))