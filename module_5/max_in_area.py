def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def side_triangles(matrix):
    length = len(matrix)
    result = []
    for col in range(length):
        for row in range(length):
            if row >= length - 1 - col:
                result.append(matrix[row][col])
    return max(result)

matrix = make_matrix(int(input()))
print(side_triangles(matrix))