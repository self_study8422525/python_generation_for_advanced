def make_matrix(size):
    return [list(map(int, input().split())) for _ in range(size)]


def is_symmetric(input_matrix):
    length = len(input_matrix)

    for row in range(length):
        for col in range(length):
            if row + col > len(input_matrix) - 1:
                if input_matrix[row][col] != input_matrix[length - col - 1][length - row - 1]:
                    return False
    return True

matrix = make_matrix(int(input()))

if is_symmetric(matrix):
    print("YES")
else:
    print("NO")
